#include<fstream>
#include<vector>
#include<string>
#include<iostream>
#include<algorithm>

std::vector<std::vector<std::string>> parse(std::string file) {
	std::ifstream in(file);
	std::string line;
	std::string curLine = "";
	int n = 0;
	std::vector<std::vector<std::string>> cur;
	std::vector<std::string> curVec;
	if (in.is_open()){
		while(getline(in, line)){
			for(auto i:line){
				if(i != '\t'){
					curLine += i;		
				}
				else{
					curVec.push_back(curLine);
					curLine = "";
				}
			}
			curVec.push_back(curLine);
			cur.push_back(curVec);
			curVec.clear();
			curLine.clear();
			
		}
		
	}
	in.close();
	return cur;
}

void myToLower(std::vector<std::vector<std::string>>& cur, int n) { 
	for(int k = 0; k < cur.size(); k++){
		transform(cur[k][n].begin(), cur[k][n].end(), cur[k][n].begin(), ::tolower);
	}
}

void myToUpper(std::vector<std::vector<std::string>>& cur, int n) {
	for(int k = 0; k < cur.size(); k++){
		transform(cur[k][n].begin(), cur[k][n].end(), cur[k][n].begin(), ::toupper);
	}
}

void myChange(std::vector<std::vector<std::string>>& cur, int n, char from, char to) {
	for (int k = 0; k < cur.size(); k++) {
		for (int j = 0; j < cur[k][n].size(); j++) {
			if (cur[k][n][j] == from){
				cur[k][n][j] = to;
			}
		}
	}
}

void myPrint(std::vector<std::vector<std::string>> cur) {
	for(auto i:cur) {
		for (auto j:i){
			std::cout << j << '\t';
		}
		std::cout << std::endl;
	}
}

int main(int argc, char * argv[]) {
	std::vector<std::vector<std::string>> cur;
	cur = parse(argv[1]);
	int i = 2;
	while (i < argc) {
		if (argv[i][2] == 'u') {
			int n = argv[i][0] - '0';
			myToLower(cur, n);
		}
		if (argv[i][2] == 'U') {
			int n = argv[i][0] - '0';
			myToUpper(cur, n);
		}
		if (argv[i][2] == 'R') {
			int n = argv[i][0] - '0';
			char from = argv[i][3];
			char to = argv[i][4];
			myChange(cur, n, from, to);
		}
		i++;
	}
	myPrint(cur);
	return 0;	
}
	
	
	


	
